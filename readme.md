# Staty - A Smart .NET State Machine #

Staty is a smart state-machine for .NET. Written as C# portable class library (PCL), Staty comes as an expressive event-driven state-machine. Staty has influences from other state machine frameworks, particularly by [David Lafreniere's C++ State Machine](http://www.drdobbs.com/cpp/state-machine-design-in-c/184401236). 

[![Build status](https://ci.appveyor.com/api/projects/status/iy1lrc4cepvs6amx?svg=true)](https://ci.appveyor.com/project/apacha/staty)
[![NuGet version](https://img.shields.io/nuget/v/Staty.svg?style=flat-square)](https://www.nuget.org/packages/Staty)

## Features ##

- Event-driven deterministic state-machine
- Concurrency-safe
- External and internal events
- Separated class per state
- Entry- and Exit actions with custom strongly typed event-data
- Easy and concise configuration with fluent syntax
- Meaningful exceptions for common configuration errors (e.g. forgetting to declare a possible transition)

## Example ##
This example demonstrates the usage of a simple motor-state-machine. The full example can be found under MotorSample

![Motor State Machine.png](https://bitbucket.org/repo/XEqnGG/images/3350844-Motor%20State%20Machine.png)

```
#!c#
// ====================================
// == The final state-machine in use ==
// ====================================
MotorStateMachine motorStateMachine = new MotorStateMachine();
motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(100));
motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(200));
motorStateMachine.ExternalEvent(MotorEvents.Halt, null);   

// ============
// == Output ==
// ============
// Automatic transition to initial state
Entering Idle state with event Halt 

// SetSpeed(100)
Exiting Idle state
Entering Start state with event SetSpeed and setting speed to 100

// SetSpeed(200)
Exiting Start state
Entering ChangeSpeed state with event SetSpeed and setting speed to 200

// Halt
Exiting ChangeSpeed state
Entering Stop state with event Halt
// Internal transition triggered in Enter-method of Stop-state
Exiting Stop state
Entering Idle state with event MotorStopped 
    
```

## How to use it ##

### 1. Get it ###
```
#!shell
nuget install staty
```

### 2. Define possible states, events and event-data ###
```
#!c#
public enum MotorState
{
    Idle,
    Stop,
    Start,
    ChangeSpeed
}

public enum MotorEvents
{
    Halt,
    SetSpeed,
    MotorStopped // Internal event
}

public class MotorData
{
    public int MotorSpeed { get; private set; }
}
```   

### 3. Create states - one class per state ###
```
#!c#
public class Idle : State<MotorState, MotorEvents, MotorData>
{
    public Idle(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
        : base(MotorState.Idle, motorStateMachine)
    {
    }

    ...
}

public class ChangeSpeed : State<MotorState, MotorEvents, MotorData>
{
    public ChangeSpeed(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
        : base(MotorState.ChangeSpeed, motorStateMachine)
    {
    }

    public override void Enter(MotorEvents transitionEvent, MotorData eventData)
    {
        Console.WriteLine("Entering ChangeSpeed state with event " + transitionEvent + " and setting speed to " + eventData.MotorSpeed);
    }

    ...
}

public class Start : State<MotorState, MotorEvents, MotorData>
{
    public Start(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
        : base(MotorState.Start, motorStateMachine)
    {
    }

    ...
}

public class Stop : State<MotorState, MotorEvents, MotorData>
{
    public Stop(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
        : base(MotorState.Stop, motorStateMachine)
    {
    }

    public override void Enter(MotorEvents transitionEvent, MotorData eventData)
    {
        StateTransitioner.InternalEvent(MotorEvents.MotorStopped, eventData);
    }

    ...
}
```   

### 4. Implement StateProvider ###
Depending on your application, the states might have more dependencies that can be injected in the StateProvider.
Otherwise you simply return a list of all states.

```
#!c#
public class MotorStateProvider : StateProvider<State<MotorState, MotorEvents>, MotorState, MotorEvents, MotorData>
{
    protected override State<MotorState, MotorEvents, MotorData>[] GetStates(IStateTransitioner<MotorEvents, MotorData> stateTransitioner)
    {
        return new State<MotorState, MotorEvents, MotorData>[]
                   { 
                       new Idle(stateTransitioner), 
                       new Stop(stateTransitioner), 
                       new Start(stateTransitioner), 
                       new ChangeSpeed(stateTransitioner) 
                   };
    }
}
```   

### 5a. Configure your state-machine ###

This configuration can be ordered by the different states in which events can occur. Use this style, if the states are the main focus of your application.

```
#!c#
public class MotorStateMachine : StateMachine<State<MotorState, MotorEvents, MotorData>, MotorState, MotorEvents, MotorData>
{
    public MotorStateMachine()
        : base(MotorStateMachineConfiguration, new MotorStateProvider(), MotorState.Idle, null)
    {
    }

    private static IStateMachineConfiguration<MotorState, MotorEvents, MotorData> MotorStateMachineConfiguration2
    {
        get
        {
            var configuration = new StateMachineConfigurationBuilder<MotorState, MotorEvents, MotorData>()
            .WhenIn(MotorState.Idle).AndReceived(MotorEvents.Halt).IgnoreEvent()
            .WhenIn(MotorState.Idle).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.Start)

            .WhenIn(MotorState.Start).AndReceived(MotorEvents.Halt).GoTo(MotorState.Stop)
            .WhenIn(MotorState.Start).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.ChangeSpeed)

            .WhenIn(MotorState.Stop).AndReceived(MotorEvents.Halt).ThrowInvalidTransitionException()
            .WhenIn(MotorState.Stop).AndReceived(MotorEvents.SetSpeed).ThrowInvalidTransitionException()
            .WhenIn(MotorState.Stop).AndReceived(MotorEvents.MotorStopped).GoTo(MotorState.Idle)

            .WhenIn(MotorState.ChangeSpeed).AndReceived(MotorEvents.Halt).GoTo(MotorState.Stop)
            .WhenIn(MotorState.ChangeSpeed).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.ChangeSpeed)
            .Build();

            return configuration;
        }
    }
}

```   

### 5b. (Alternatively) Configure your state-machine ###

This configuration highlights the possible events and can be ordered by them. Use this style, if the events are the main focus in your application.

```
#!c#
public class MotorStateMachine : StateMachine<State<MotorState, MotorEvents, MotorData>, MotorState, MotorEvents, MotorData>
{
    public MotorStateMachine()
        : base(MotorStateMachineConfiguration, new MotorStateProvider(), MotorState.Idle, null)
    {
    }

    private static IStateMachineConfiguration<MotorState, MotorEvents, MotorData> MotorStateMachineConfiguration
    {
        get
        {
            var configuration = new StateMachineConfigurationBuilder<MotorState, MotorEvents, MotorData>()
            .When(MotorEvents.Halt).HappensIn(MotorState.Idle).IgnoreEvent()
            .When(MotorEvents.Halt).HappensIn(MotorState.Stop).ThrowInvalidTransitionException()
            .When(MotorEvents.Halt).HappensIn(MotorState.Start).GoTo(MotorState.Stop)
            .When(MotorEvents.Halt).HappensIn(MotorState.ChangeSpeed).GoTo(MotorState.Stop)

            .When(MotorEvents.SetSpeed).HappensIn(MotorState.Idle).GoTo(MotorState.Start)
            .When(MotorEvents.SetSpeed).HappensIn(MotorState.Stop).ThrowInvalidTransitionException()
            .When(MotorEvents.SetSpeed).HappensIn(MotorState.Start).GoTo(MotorState.ChangeSpeed)
            .When(MotorEvents.SetSpeed).HappensIn(MotorState.ChangeSpeed).GoTo(MotorState.ChangeSpeed)

            .When(MotorEvents.MotorStopped).HappensIn(MotorState.Stop).GoTo(MotorState.Idle)
            .Build();

            return configuration;
        }
    }
}

```   
		

### 6. Use your state-machine ###
```
#!c#
MotorStateMachine motorStateMachine = new MotorStateMachine();
motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(100));
motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(200));
motorStateMachine.ExternalEvent(MotorEvents.Halt, null);  
```   

## Contributing ##

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## License & Copyright ##
Released under the MIT license.

Copyright, 2016, by [Alexander Pacha](http://my-it.at).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.