﻿namespace MotorSample
{
    using MotorSample.Configuration;

    class Program
    {
        static void Main(string[] args)
        {
            var motorStateMachine = new MotorStateMachine();
            motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(100));
            motorStateMachine.ExternalEvent(MotorEvents.SetSpeed, new MotorData(200));
            motorStateMachine.ExternalEvent(MotorEvents.Halt, null);                        
        }
    }
}
