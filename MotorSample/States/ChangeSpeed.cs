﻿namespace MotorSample.States
{
    using System;

    using MotorSample.Configuration;
    using Staty;

    public class ChangeSpeed : State<MotorState, MotorEvents, MotorData>
    {
        public ChangeSpeed(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
            : base(MotorState.ChangeSpeed, motorStateMachine)
        {
        }

        public override void Enter(MotorEvents transitionEvent, MotorData eventData)
        {
            Console.WriteLine("Entering ChangeSpeed state with event " + transitionEvent + " and setting speed to " + eventData.MotorSpeed);
        }

        public override void Exit(MotorEvents transitionEvent)
        {
            Console.WriteLine("Exiting ChangeSpeed state");
        }
    }
}