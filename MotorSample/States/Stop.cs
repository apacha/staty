namespace MotorSample.States
{
    using System;

    using MotorSample.Configuration;
    using Staty;

    public class Stop : State<MotorState, MotorEvents, MotorData>
    {
        public Stop(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
            : base(MotorState.Stop, motorStateMachine)
        {
        }

        public override void Enter(MotorEvents transitionEvent, MotorData eventData)
        {
            Console.WriteLine("Entering Stop state with event " + transitionEvent);
            // Actually stop the motor here
            StateTransitioner.InternalEvent(MotorEvents.MotorStopped, eventData);
        }

        public override void Exit(MotorEvents transitionEvent)
        {
            Console.WriteLine("Exiting Stop state");
        }
    }
}