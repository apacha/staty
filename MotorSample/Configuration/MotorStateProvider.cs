namespace MotorSample.Configuration
{
    using MotorSample.States;
    using Staty;

    public class MotorStateProvider : StateProvider<State<MotorState, MotorEvents, MotorData>, MotorState, MotorEvents, MotorData>
    {
        protected override State<MotorState, MotorEvents, MotorData>[] GetStates(IStateTransitioner<MotorEvents, MotorData> stateTransitioner)
        {
            return new State<MotorState, MotorEvents, MotorData>[]
                       { 
                           new Idle(stateTransitioner), 
                           new Stop(stateTransitioner), 
                           new Start(stateTransitioner), 
                           new ChangeSpeed(stateTransitioner) };
        }
    }
}