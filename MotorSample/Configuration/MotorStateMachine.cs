﻿namespace MotorSample.Configuration
{
    using Staty;
    using Staty.Configuration;

    public class MotorStateMachine : StateMachine<State<MotorState, MotorEvents, MotorData>, MotorState, MotorEvents, MotorData>
    {
        public MotorStateMachine()
            : base(MotorStateMachineConfiguration2, new MotorStateProvider(), MotorState.Idle, null)
        {
        }

        private static IStateMachineConfiguration<MotorState, MotorEvents, MotorData> MotorStateMachineConfiguration
        {
            get
            {
                var configuration = new StateMachineConfigurationBuilder<MotorState, MotorEvents, MotorData>()
                .When(MotorEvents.Halt).HappensIn(MotorState.Idle).IgnoreEvent()
                .When(MotorEvents.Halt).HappensIn(MotorState.Stop).ThrowInvalidTransitionException()
                .When(MotorEvents.Halt).HappensIn(MotorState.Start).GoTo(MotorState.Stop)
                .When(MotorEvents.Halt).HappensIn(MotorState.ChangeSpeed).GoTo(MotorState.Stop)
                .When(MotorEvents.SetSpeed).HappensIn(MotorState.Idle).GoTo(MotorState.Start)
                .When(MotorEvents.SetSpeed).HappensIn(MotorState.Stop).ThrowInvalidTransitionException()
                .When(MotorEvents.SetSpeed).HappensIn(MotorState.Start).GoTo(MotorState.ChangeSpeed)
                .When(MotorEvents.SetSpeed).HappensIn(MotorState.ChangeSpeed).GoTo(MotorState.ChangeSpeed)
                .When(MotorEvents.MotorStopped).HappensIn(MotorState.Stop).GoTo(MotorState.Idle)
                .Build();

                return configuration;
            }
        }

        private static IStateMachineConfiguration<MotorState, MotorEvents, MotorData> MotorStateMachineConfiguration2
        {
            get
            {
                var configuration = new StateMachineConfigurationBuilder<MotorState, MotorEvents, MotorData>()
                .WhenIn(MotorState.Idle).AndReceived(MotorEvents.Halt).IgnoreEvent()
                .WhenIn(MotorState.Stop).AndReceived(MotorEvents.Halt).ThrowInvalidTransitionException()
                .WhenIn(MotorState.Start).AndReceived(MotorEvents.Halt).GoTo(MotorState.Stop)
                .WhenIn(MotorState.ChangeSpeed).AndReceived(MotorEvents.Halt).GoTo(MotorState.Stop)
                .WhenIn(MotorState.Idle).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.Start)
                .WhenIn(MotorState.Stop).AndReceived(MotorEvents.SetSpeed).ThrowInvalidTransitionException()
                .WhenIn(MotorState.Start).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.ChangeSpeed)
                .WhenIn(MotorState.ChangeSpeed).AndReceived(MotorEvents.SetSpeed).GoTo(MotorState.ChangeSpeed)
                .WhenIn(MotorState.Stop).AndReceived(MotorEvents.MotorStopped).GoTo(MotorState.Idle)
                .Build();

                return configuration;
            }
        }
    }
}