﻿namespace Staty
{
    using Staty.Configuration;

    /// <summary>
    /// Basic data transfer object for transitions
    /// </summary>
    internal class Transition<TEventEnum, TStateEnum, TEventData>
        where TEventEnum : struct
        where TStateEnum : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Transition{TEventEnum, TStateEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="occurredEvent">The event that occurred</param>
        /// <param name="transitionAction">The transition action that should be executed</param>
        /// <param name="eventData">An optional payload</param>
        internal Transition(TEventEnum occurredEvent, TransitionAction<TStateEnum, TEventData> transitionAction, TEventData eventData)
        {
            OccurredEvent = occurredEvent;
            TransitionAction = transitionAction;
            EventData = eventData;
        }

        /// <summary>
        /// Gets the event that occurred
        /// </summary>
        public TEventEnum OccurredEvent { get; private set; }

        /// <summary>
        /// Gets the transition action that should be executed
        /// </summary>
        public TransitionAction<TStateEnum, TEventData> TransitionAction { get; private set; }

        /// <summary>
        /// Gets an optional payload
        /// </summary>
        public TEventData EventData { get; private set; }
    }
}