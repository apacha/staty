namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that will be raised, if a configuration is tried to added twice
    /// </summary>
    public class DuplicationConfigurationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicationConfigurationException"/> class.
        /// </summary>
        public DuplicationConfigurationException()
            : base("Non deterministic state-machine is not supported. Every '(event, sourceState) -> destinationState' mapping entry must be unique.")
        {
        }
    }
}