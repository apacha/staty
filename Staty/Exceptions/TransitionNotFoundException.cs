namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception, that will be thrown, if a requested transition was not found in the state-machine configuration
    /// </summary>
    public class TransitionNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransitionNotFoundException"/> class.
        /// </summary>
        /// <param name="eventName">Name of the event that was not found in the configuration</param>
        public TransitionNotFoundException(string eventName)
            : base(
                "The transition for event " + eventName
                + " could not be found in the StateMachineConfiguration. This typically happens, when the state machine configuration is incomplete")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransitionNotFoundException"/> class.
        /// </summary>
        /// <param name="eventName">Name of the event that was not found in the configuration</param>
        /// <param name="currentStateName">The name of the state that the state-machine was currently in</param>
        public TransitionNotFoundException(string eventName, string currentStateName)
            : base(
                "The transition for event " + eventName + " from state " + currentStateName
                + " could not be found in the StateMachineConfiguration. This typically happens, when the state machine configuration is incomplete")
        {
        }
    }
}