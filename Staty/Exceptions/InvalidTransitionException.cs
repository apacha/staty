namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that will be thrown, if an invalid transition was attempted
    /// </summary>
    public class InvalidTransitionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidTransitionException"/> class.
        /// </summary>
        /// <param name="currentState">The current state of the state-machine</param>
        /// <param name="eventName">The event that occurred but was not allowed</param>
        public InvalidTransitionException(string currentState, string eventName)
            : base($"Requested transition from event {eventName} in state {currentState} is not permitted")
        {
        }
    }
}