namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that will be thrown, if the state provider was not correctly initialized
    /// </summary>
    public class NotInitializedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotInitializedException"/> class.
        /// </summary>
        public NotInitializedException()
            : base("The state machine is not correctly initialized")
        {            
        }
    }
}