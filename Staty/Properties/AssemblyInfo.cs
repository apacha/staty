﻿using System.Reflection;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Staty - A Smart .NET State Machine")]
[assembly: AssemblyDescription("Staty is a smart state-machine for .NET. Written as C# portable class library (PCL), Staty comes as an expressive event-driven state-machine with features like concurrence-safety, external and interal events, separate classes per state, entry and exit actions and a fluent syntax to configure the state-machine.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alexander Pacha")]
[assembly: AssemblyProduct("Staty")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.3.0")]
[assembly: AssemblyFileVersion("2.1.3.0")]
