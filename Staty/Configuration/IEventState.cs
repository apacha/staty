﻿namespace Staty.Configuration
{
    /// <summary>
    /// Interface that only allows to configure the event to receive in a fluent builder-pattern
    /// </summary>
    public interface IEventState<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Starts a new configuration-entry by specifying the event that must happen in a source-state to trigger an action
        /// </summary>
        /// <param name="eventEnum">The event that must occur</param>
        /// <returns>An object for the next step of the builder-pattern</returns>
        IAction<TStateEnum, TEventEnum, TEventData> AndReceived(TEventEnum eventEnum);
    }
}