﻿namespace Staty.Configuration
{
    using System;

    /// <summary>
    /// An action that does nothing upon execution
    /// </summary>
    public class IgnoreEventAction<TStateEnum, TEventData> : TransitionAction<TStateEnum, TEventData>
        where TStateEnum : struct
    {
        /// <inheritdoc/>
        public override void PerformTransition<TEventEnum>(Action<TStateEnum, TEventEnum, TEventData> transitionAction, TEventEnum occurredEvent, TEventData eventData)
        {
            // Do no perform the transition action
        }
    }
}