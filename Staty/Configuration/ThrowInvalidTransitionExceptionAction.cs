namespace Staty.Configuration
{
    using System;
    using Staty.Exceptions;

    /// <summary>
    /// A transition action that will deliberately throw an exception, if the transition is executed
    /// </summary>
    /// <typeparam name="TStateEnum">The enumeration which contains a values for each available state</typeparam>
    /// <typeparam name="TEventData">The type of the event-data that can be passed to the state-machine</typeparam>
    public class ThrowInvalidTransitionExceptionAction<TStateEnum, TEventData> : TransitionAction<TStateEnum, TEventData>
        where TStateEnum : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ThrowInvalidTransitionExceptionAction{TStateEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="stateName">How this name will be refered to in the state-machine</param>
        public ThrowInvalidTransitionExceptionAction(string stateName)
        {
            StateName = stateName;
        }

        /// <summary>
        /// Gets the name of the source-state that the state machine was in when the prohibited transition was triggered
        /// </summary>
        public string StateName { get; }

        /// <inheritdoc/>
        public override void PerformTransition<TEventEnum>(Action<TStateEnum, TEventEnum, TEventData> transitionAction, TEventEnum occurredEvent, TEventData eventData)
        {
            throw new InvalidTransitionException(StateName, occurredEvent.ToString());
        }
    }
}