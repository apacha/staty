﻿namespace Staty.Configuration
{
    using System.Collections.Generic;

    using Staty.Exceptions;

    /// <summary>
    /// Class that implements a builder-pattern for creating a configuration of the state-machine in a fluent syntax.
    /// The original idea comes from http://blog.crisp.se/2013/10/09/perlundholm/another-builder-pattern-for-java
    /// </summary>
    /// <typeparam name="TStateEnum">The enumeration which contains a values for each available state</typeparam>
    /// <typeparam name="TEventEnum">The enumeration which contains all possible events (internal and external) that this state-machine should be able to process.</typeparam>
    /// <typeparam name="TEventData">The type of the event-data that can be passed to the state-machine</typeparam>
    public class StateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> 
        : IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData>,
          ISourceState<TStateEnum, TEventEnum, TEventData>,
          IEventState<TStateEnum, TEventEnum, TEventData>,
          IAction<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        private readonly IDictionary<TEventEnum, IDictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>>> _eventTransitionMap;

        private TEventEnum _whenEvent;

        private TStateEnum _happensInState;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateMachineConfigurationBuilder{TStateEnum, TEventEnum, TEventData}"/> class
        /// that can be used to created a state-machine configuration with a fluent syntax.
        /// </summary>
        public StateMachineConfigurationBuilder()
        {
            _eventTransitionMap = new Dictionary<TEventEnum, IDictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>>>();
        }

        /// <inheritdoc/>
        public IStateMachineConfiguration<TStateEnum, TEventEnum, TEventData> Build()
        {
            return new StateMachineConfiguration<TStateEnum, TEventEnum, TEventData>(_eventTransitionMap);
        }

        /// <inheritdoc/>
        public ISourceState<TStateEnum, TEventEnum, TEventData> When(TEventEnum eventEnum)
        {
            _whenEvent = eventEnum;
            return this;
        }

        /// <inheritdoc/>
        public IEventState<TStateEnum, TEventEnum, TEventData> WhenIn(TStateEnum sourceState)
        {
            _happensInState = sourceState;
            return this;
        }

        /// <inheritdoc/>
        public IAction<TStateEnum, TEventEnum, TEventData> HappensIn(TStateEnum sourceState)
        {
            _happensInState = sourceState;
            return this;
        }

        /// <inheritdoc/>
        public IAction<TStateEnum, TEventEnum, TEventData> AndReceived(TEventEnum eventEnum)
        {
            _whenEvent = eventEnum;
            return this;
        }

        /// <inheritdoc/>
        public IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> GoTo(TStateEnum destinationState)
        {
            EnsureEventTransitionMapHasEventEntry(_whenEvent);
            var transitionMapForASingleEvent = _eventTransitionMap[_whenEvent];
            EnsureEventTransitionMapHasNoDuplicate(transitionMapForASingleEvent, _happensInState);
            transitionMapForASingleEvent.Add(_happensInState, new GoToAction<TStateEnum, TEventData>(destinationState));
            return this;
        }

        /// <inheritdoc/>
        public IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> ThrowInvalidTransitionException()
        {
            EnsureEventTransitionMapHasEventEntry(_whenEvent);
            var transitionMapForASingleEvent = _eventTransitionMap[_whenEvent];
            EnsureEventTransitionMapHasNoDuplicate(transitionMapForASingleEvent, _happensInState);
            transitionMapForASingleEvent.Add(_happensInState, new ThrowInvalidTransitionExceptionAction<TStateEnum, TEventData>(_happensInState.ToString()));
            return this;
        }

        /// <inheritdoc/>
        public IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> IgnoreEvent()
        {
            EnsureEventTransitionMapHasEventEntry(_whenEvent);
            var transitionMapForASingleEvent = _eventTransitionMap[_whenEvent];
            EnsureEventTransitionMapHasNoDuplicate(transitionMapForASingleEvent, _happensInState);
            transitionMapForASingleEvent.Add(_happensInState, new IgnoreEventAction<TStateEnum, TEventData>());
            return this;
        }

        /// <summary>
        /// Create the event-entry in the dictionary if it does not exist yet
        /// </summary>
        /// <param name="eventEnum">The event that should be checked for an existing entry in the transition map</param>
        private void EnsureEventTransitionMapHasEventEntry(TEventEnum eventEnum)
        {
            if (_eventTransitionMap.ContainsKey(eventEnum))
            {
                return;
            }

            _eventTransitionMap.Add(eventEnum, new Dictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>>());
        }

        private void EnsureEventTransitionMapHasNoDuplicate(IDictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>> eventTransitionMap, TStateEnum stateEnum)
        {
            if (eventTransitionMap.ContainsKey(stateEnum))
            {
                throw new DuplicationConfigurationException();
            }
        }
    }
}