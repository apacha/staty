﻿namespace Staty.Configuration
{
    /// <summary>
    /// Interface of the basic state-machine configuration builder that allows
    /// to configure and build the configuration in a fluent syntax.
    /// </summary>
    public interface IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Call this method to complete the construction and return a new state-machine configuration with the 
        /// event-transition map that was configured with previous calls.
        /// </summary>
        /// <returns>A state-machine configuration</returns>
        IStateMachineConfiguration<TStateEnum, TEventEnum, TEventData> Build();

        /// <summary>
        /// Starts a new configuration-entry by specifying the event that must happen in a source-state to trigger an action
        /// </summary>
        /// <param name="eventEnum">The event that must occur</param>
        /// <returns>An object for the next step of the builder-pattern</returns>
        ISourceState<TStateEnum, TEventEnum, TEventData> When(TEventEnum eventEnum);

        /// <summary>
        /// Starts a new configuration-entry by specifying the state, in which an event must happen to trigger an action.
        /// </summary>
        /// <param name="sourceState">The state in which an event has to happen</param>
        /// <returns>An object for the next step of the builder-pattern</returns>
        IEventState<TStateEnum, TEventEnum, TEventData> WhenIn(TStateEnum sourceState);
    }
}