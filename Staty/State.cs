﻿namespace Staty
{
    using System;

    /// <summary>
    /// Abstract base class for state of the state-machine.
    /// Note that states are uniquely identified by its name.
    /// </summary>
    public abstract class State<TStateEnum, TEventEnum, TEventData> : IState<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="State{TStateEnum, TEventEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="stateEnum">The state-enum this state is associated with (should be statically typed)</param>
        /// <param name="stateTransitioner">The state-machine</param>
        protected State(TStateEnum stateEnum, IStateTransitioner<TEventEnum, TEventData> stateTransitioner)
        {
            StateTransitioner = stateTransitioner;
            StateEnum = stateEnum;
            Name = GetType().AssemblyQualifiedName;
        }

        /// <inheritdoc/>
        public string Name { get; }

        /// <inheritdoc/>
        public TStateEnum StateEnum { get; }

        /// <summary>
        /// Gets the reference to the state-machine (the event-processor) to enable internal events from inside of states
        /// </summary>
        protected IStateTransitioner<TEventEnum, TEventData> StateTransitioner { get; private set; }

        /// <summary>
        /// Method that automatically will be called, when this state is entered.
        /// Override this method to implement your own enter-logic.
        /// </summary>
        /// <param name="transitionEvent">The event that caused this transition</param>
        /// <param name="eventData">The optional event-data that was provided</param>
        public virtual void Enter(TEventEnum transitionEvent, TEventData eventData)
        {
            // Intentionally left blank, because states can ignore their enter-event
        }

        /// <summary>
        /// Method that automatically will be called, when this state is exited.
        /// Override this method to implement your own exit-logic.
        /// </summary>
        /// <param name="transitionEvent">The event that caused that this state is left</param>
        public virtual void Exit(TEventEnum transitionEvent)
        {
            // Intentionally left blank, because states can ignore their exit-event
        }

        /// <summary>
        /// Implementation of the disposable-pattern
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Override this method to actually implement a disposing in your state
        /// </summary>
        /// <param name="disposing">True, if the disposing should happen now</param>
        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Checks if the given state is equal to this state
        /// </summary>
        /// <param name="other">The other state</param>
        /// <returns>True if they are considered equal</returns>
        protected bool Equals(State<TStateEnum, TEventEnum, TEventData> other)
        {
            return string.Equals(Name, other.Name);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((State<TStateEnum, TEventEnum, TEventData>)obj);
        }

        /// <summary>
        /// Checks if two states are considered equal
        /// </summary>
        /// <param name="left">The first state</param>
        /// <param name="right">The second state</param>
        /// <returns>True if they are considered equal</returns>
        public static bool operator ==(State<TStateEnum, TEventEnum, TEventData> left, State<TStateEnum, TEventEnum, TEventData> right)
        {
            return Equals(left, right);
        }

        /// <summary>        
        /// /// Checks if two states are considered not equal
        /// </summary>
        /// <param name="left">The first state</param>
        /// <param name="right">The second state</param>
        /// <returns>False if they are considered equal</returns>
        public static bool operator !=(State<TStateEnum, TEventEnum, TEventData> left, State<TStateEnum, TEventEnum, TEventData> right)
        {
            return !Equals(left, right);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"State[Name: {Name}]";
        }
    }
}
