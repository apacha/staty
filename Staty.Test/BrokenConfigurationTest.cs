namespace StatyTest
{
    using FakeItEasy;

    using NUnit.Framework;

    using Staty;
    using Staty.Configuration;
    using Staty.Exceptions;

    using StatyTest.TestClasses;

    using TestState = StatyTest.TestClasses.TestState;

    public class BrokenConfigurationTest
    {
        [Test]
        public void BrokenConfiguration_ProvideTwoStatesWithSameStateEnum_ExpectProperException()
        {
            // Arrange
            var stateTransitioner = A.Fake<IStateTransitioner<TestEventEnum, TestEventData>>();
            var state3 = new State3(stateTransitioner);
            var state4WithState3Enum = new State4WithState3Enum(stateTransitioner);
            var stateMachineConfiguration = A.Fake<IStateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData>>();

            IStateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData> stateProvider = new TestStateProvider(
                new TestState[] { state3, state4WithState3Enum });

            // Act & Assert
            Assert.That(() => new TestStateMachine(stateMachineConfiguration, stateProvider, TestStateEnum.State1), Throws.InstanceOf<StateEnumMustBeUniqueException>());

            // Assert
        }
    }
}