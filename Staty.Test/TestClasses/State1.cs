namespace StatyTest.TestClasses
{
    using Staty;

    public class State1 : TestState
    {
        public bool Disposed { get; set; }
        public State1(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
            : base(stateTransitioner, TestStateEnum.State1)
        {
        }

        protected override void Dispose(bool disposing)
        {
            Disposed = true;
        }
    }
}