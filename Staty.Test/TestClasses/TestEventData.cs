﻿namespace StatyTest.TestClasses
{
    using Staty;

    public class TestEventData
    {
        public TestEventData(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }
}