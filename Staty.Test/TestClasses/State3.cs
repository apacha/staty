namespace StatyTest.TestClasses
{
    using Staty;

    public class State3 : TestState
    {
        public State3(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
            : base(stateTransitioner, TestStateEnum.State3)
        {
        }

        public override void Enter(TestEventEnum transitionEvent, TestEventData eventData)
        {
            StateTransitioner.InternalEvent(TestEventEnum.InternalEvent1, eventData);
        }
    }
}