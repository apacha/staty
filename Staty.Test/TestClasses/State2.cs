namespace StatyTest.TestClasses
{
    using Staty;

    public class State2 : TestState
    {
        public bool Disposed { get; set; }

        public TestEventData EventData { get; set; }

        public State2(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
            : base(stateTransitioner, TestStateEnum.State2)
        {
        }

        public override void Enter(TestEventEnum transitionEvent, TestEventData eventData)
        {
            EventData = (TestEventData)eventData;
        }

        protected override void Dispose(bool disposing)
        {
            Disposed = true;
        }
    }
}