﻿namespace StatyTest
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using FakeItEasy;
    using NUnit.Framework;
    using Staty;
    using Staty.Configuration;
    using Staty.Exceptions;
    using StatyTest.TestClasses;
    using TestState = StatyTest.TestClasses.TestState;

    [TestFixture]
    public class StateMachineInfrastructureTest
    {
        private IStateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData> _fakeStateProvider;

        private IStateTransitioner<TestEventEnum, TestEventData> _fakeStateTransitioner;

        private IStateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData> _fakeStateMachineConfiguration;

        [SetUp]
        public void Setup()
        {
            _fakeStateTransitioner = A.Fake<IStateTransitioner<TestEventEnum, TestEventData>>();
            _fakeStateProvider = A.Fake<IStateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData>>();
            _fakeStateMachineConfiguration = A.Fake<IStateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData>>();
        }

        [Test]
        public void CreateStateMachine_ExpectInitialState()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            // Act
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            stateMachine.ExternalEvent(TestEventEnum.Event1, new TestEventData(100));

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));            
        }

        [Test]
        public void DisposeStateMachine_ExpectDisposeCallInAllStates()
        {
            // Arrange
            var state1 = new State1(_fakeStateTransitioner);
            var state2 = new State2(_fakeStateTransitioner);
            A.CallTo(() => _fakeStateProvider.Dispose()).Invokes(
                () =>
                    {
                        state1.Dispose();
                        state2.Dispose();
                    });
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(state1);
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(state2);
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            Assert.That(state1.Disposed, Is.False);
            Assert.That(state2.Disposed, Is.False);

            // Act
            stateMachine.Dispose();

            // Assert
            Assert.That(state1.Disposed, Is.True);
            Assert.That(state2.Disposed, Is.True);
        }
        
        [Test]
        public void TriggerTransitions_TriggerValidTransition_ExpectChangedState()
        {
            // Arrange
            var anyNumber = 42;
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            var state2 = new State2(_fakeStateTransitioner);
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(state2);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act
            stateMachine.ExternalEvent(TestEventEnum.Event1, new TestEventData(anyNumber));

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));
            Assert.That(state2.EventData.Value, Is.EqualTo(anyNumber));
        }

        [Test]
        public void TriggerTransitions_TriggerValidTransition_ExpectEnterAndExitActions()
        {
            // Arrange
            var state1 = A.Fake<State1>();
            var state2 = A.Fake<State2>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(state1);
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(state2);
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            // Act
            stateMachine.ExternalEvent(TestEventEnum.Event1, null);

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));
            A.CallTo(() => state1.Enter(A<TestEventEnum>.Ignored, A<TestEventData>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => state1.Exit(A<TestEventEnum>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => state2.Enter(A<TestEventEnum>.Ignored, A<TestEventData>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void TriggerTransitions_TriggerValidTransition_ExpectSpecificEnterAndExitActions()
        {
            // Arrange
            var state1 = A.Fake<State1>();
            var state2 = A.Fake<State2>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(state1);
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(state2);
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event2)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            // Act
            stateMachine.ExternalEvent(TestEventEnum.Event2, null);

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));
            A.CallTo(() => state1.Enter(A<TestEventEnum>.Ignored, A<TestEventData>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => state1.Exit(TestEventEnum.Event2)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => state2.Enter(TestEventEnum.Event2, A<TestEventData>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void TriggerTransitions_TriggerMultipleTransition_ExpectChangedState()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State2, TestEventEnum.Event2)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State1));

            // Act
            stateMachine.ExternalEvent(TestEventEnum.Event1, null);
            stateMachine.ExternalEvent(TestEventEnum.Event2, null);
            stateMachine.ExternalEvent(TestEventEnum.Event1, null);

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));
        }

        [Test]
        public void TriggerTransitions_TriggerSelfTransition_ExpectPerformedSelfTransition()
        {
            // Arrange
            var fakeState1 = A.Fake<State1>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(fakeState1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State1));
            var stateMachineThatAllowsSelfTransition = 
                new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act & Assert
            Assert.That(() => stateMachineThatAllowsSelfTransition.ExternalEvent(TestEventEnum.Event1, null), Throws.Nothing);
            A.CallTo(() => fakeState1.Enter(TestEventEnum.Event1, null)).MustHaveHappened(Repeated.Exactly.Twice);
        }

        [Test]
        public void TriggerTransitions_TriggerSelfTransition_ExpectIgnoredSelfTransition()
        {
            // Arrange
            var fakeState1 = A.Fake<State1>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(fakeState1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new IgnoreEventAction<TestStateEnum, TestEventData>());
            var stateMachineThatAllowsSelfTransition =
                new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act & Assert
            Assert.That(() => stateMachineThatAllowsSelfTransition.ExternalEvent(TestEventEnum.Event1, null), Throws.Nothing);
            A.CallTo(() => fakeState1.Enter(TestEventEnum.Event1, null)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void TriggerTransitions_TriggerSelfTransition_ExpectExceptionUponSelfTransition()
        {
            // Arrange
            var fakeState1 = A.Fake<State1>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(fakeState1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1))
                .Returns(new ThrowInvalidTransitionExceptionAction<TestStateEnum, TestEventData>(""));
            var stateMachineThatAllowsSelfTransition =
                new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act & Assert
            Assert.That(() => stateMachineThatAllowsSelfTransition.ExternalEvent(TestEventEnum.Event1, null), Throws.InstanceOf<InvalidTransitionException>());
        }

        [Test]
        public void TriggerTransitions_TriggerInvalidTransition_ExpectException()
        {
            // Arrange
            var fakeState1 = A.Fake<State1>();
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(fakeState1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(A<TestStateEnum>.Ignored, A<TestEventEnum>.Ignored)).Throws<Exception>();
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act & Assert
            Assert.That(() => stateMachine.ExternalEvent(TestEventEnum.Event1, null), Throws.Exception);
            Assert.That(() => stateMachine.ExternalEvent(TestEventEnum.Event2, null), Throws.Exception);
        }

        [Test]
        public void TriggerTransitions_TriggerTransitionInFinalState_ExpectException()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State2, TestEventEnum.Event2)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State1));
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1, TestStateEnum.State2);

            stateMachine.ExternalEvent(TestEventEnum.Event1, null);

            // Act & Assert
            Assert.That(() => stateMachine.ExternalEvent(TestEventEnum.Event2, null), Throws.InstanceOf<FinalStateTransitionException>());
        }        
        
        [Test]
        public void TriggerTransitions_TriggerTransitionWhileTransitionIsHappening_ExpectExceptionDueToTimeout()
        {
            // Arrange
            var state2 = A.Fake<State2>();
            A.CallTo(() => state2.Enter(A<TestEventEnum>.Ignored, A<TestEventData>.Ignored)).Invokes(() => Thread.Sleep(500));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(state2);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State2, TestEventEnum.Event2)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State1));

            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act
            Task.Run(() => stateMachine.ExternalEvent(TestEventEnum.Event1, null)); // Start first transition
            Thread.Sleep(100);
            stateMachine.ExternalEvent(TestEventEnum.Event2, null);

            // Act & Assert
            Assert.That(stateMachine.CurrentStateEnum, Is.EqualTo(TestStateEnum.State1));
        }

        [Test]
        public void InternalTransition_SwitchToState3_ExpectedInternalTransition()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State3)).Returns(new State3(_fakeStateTransitioner));

            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State3));
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State3, TestEventEnum.InternalEvent1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateTransitioner.InternalEvent(A<TestEventEnum>.Ignored, A<TestEventData>.Ignored))
                .Invokes(() => stateMachine.InternalEvent(TestEventEnum.InternalEvent1, null));
            
            // Act
            stateMachine.ExternalEvent(TestEventEnum.Event1, null);

            // Assert
            Assert.That(stateMachine.CurrentState.StateEnum, Is.EqualTo(TestStateEnum.State2));
        }

        [Test]
        public void ExternalEvent_ThrowExceptionWhenEntering_ExpectTransitionFailedException()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State5)).Returns(new State5ThatThrowsExceptionOnEnter(_fakeStateTransitioner));

            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State5));

            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);

            // Act & Assert
            Assert.That(() => stateMachine.ExternalEvent(TestEventEnum.Event1, null), Throws.InstanceOf<TransitionFailedException>());
        }

        [Test]
        public void Dispose_ProcessExternalEvent_ExpectObjectDisposedException()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            // Act
            stateMachine.Dispose();
            
            // Assert
            Assert.That(() => stateMachine.ExternalEvent(TestEventEnum.Event1, null), Throws.InstanceOf<ObjectDisposedException>());
        }

        [Test]
        public void Dispose_ProcessInternalEvent_ExpectObjectDisposedException()
        {
            // Arrange
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State1)).Returns(new State1(_fakeStateTransitioner));
            A.CallTo(() => _fakeStateProvider.GetState(TestStateEnum.State2)).Returns(new State2(_fakeStateTransitioner));
            var stateMachine = new TestStateMachine(_fakeStateMachineConfiguration, _fakeStateProvider, TestStateEnum.State1);
            A.CallTo(() => _fakeStateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1)).Returns(new GoToAction<TestStateEnum, TestEventData>(TestStateEnum.State2));

            // Act
            stateMachine.Dispose();
            
            // Assert
            Assert.That(() => stateMachine.InternalEvent(TestEventEnum.Event1, null), Throws.InstanceOf<ObjectDisposedException>());
        }
    }
}